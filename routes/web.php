<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'BlogController@index');
Route::get('/about-us', 'BlogController@aboutUs');
Route::get('/contact-us', 'BlogController@contactUs');
Route::get('/blog-details/{id}', 'BlogController@blogDetails');
Route::get('/all-category/{id}', 'BlogController@allCategory');
Route::get('/admin/editor', 'BlogController@editor');
Route::post('/comment/{id}', 'BlogController@commentSave');



//Admin Routes
Route::get('/admin', 'AdminController@index');
Route::post('/admin/save', 'AdminController@storeAdmin');
Route::post('/admin/admin-Login-Check', 'AdminController@adminLoginCheck');
Route::get('/admin/manage-admin', 'AdminController@manageAdmin');
Route::get('/admin/authorized-admin/{id}', 'AdminController@authorizationAdmin');
Route::get('/admin/unauthorized-admin/{id}', 'AdminController@unauthorizationAdmin');
Route::get('/admin/edit-admin/{id}', 'AdminController@editAdmin');
Route::post('/admin/update-admin/{id}', 'AdminController@updateAdmin');
Route::get('/admin/delete-admin/{id}', 'AdminController@destroyAdmin');
Route::get('/admin/view-admin/{id}', 'AdminController@viewAdmin');

//SuperAdmin Routes
Route::get('/admin/dashboard', 'SuperAdminController@index');
Route::get('/logout', 'SuperAdminController@logout');
Route::get('/admin/add_catagory', 'SuperAdminController@add_catagory');
Route::get('/admin/manage_catagory', 'SuperAdminController@manage_catagory');
Route::post('/admin/save-category', 'SuperAdminController@saveCategory');
Route::get('/admin/unpublish-category/{id}', 'SuperAdminController@unpublishCategory');
Route::get('/admin/publish-category/{id}', 'SuperAdminController@publishCategory');
Route::get('/admin/view-category/{id}', 'SuperAdminController@singleViewCategory');
Route::get('/admin/edit-category/{id}', 'SuperAdminController@editCategory');
Route::post('/admin/update-category/{id}', 'SuperAdminController@updateCategory');
Route::get('/admin/delete-category/{id}', 'SuperAdminController@destroyCategory');
Route::get('/admin/add-blog', 'SuperAdminController@addBlog');
Route::post('/admin/save-blog', 'SuperAdminController@saveBlog');
Route::get('/admin/manage-blog', 'SuperAdminController@manageBlog');

Route::get('/admin/unpublish-blog/{id}', 'SuperAdminController@unpublishBlog');
Route::get('/admin/publish-blog/{id}', 'SuperAdminController@publishBlog');

//admin info routes
Route::get('/admin/add-admin', 'SuperAdminController@addAdmin');
Auth::routes();
//Other's routes Routes

//Route::get('/admin', 'AdminController@test');

