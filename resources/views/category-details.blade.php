@extends('layouts.master')

@section('main_content')

    <div class="content-grid">

        @foreach($category_details as $item)


            <div class="content-grid-info">
                <img src="{{ asset($item->blog_image) }}" width="600" height="325" alt=""/>
                <div class="post-info">
                    <h4><a href="/blog-details/{{ $item->blog_id }}">{{ $item->blog_title }} </a> July 30, 2014 / 27</h4>
                    <p>{{ $item->short_description }}</p>
                    <a href="/blog-details/{{ $item->blog_id }}"><span></span>READ MORE</a>
                </div>
            </div>
        @endforeach


    </div>
@endsection