@extends('layouts.master')

@section('main_content')

    <div class="content-grid">

            @foreach($blog_info as $item)


        <div class="content-grid-info">
            <img src="{{ asset($item->blog_image) }}" width="600" height="325" alt=""/>
            <div class="post-info">
                <h4><a href="/blog-details/{{ $item->blog_id }}">{{ $item->blog_title }} </a> July 30, 2014 / 27 {{ $item->category_name }}</h4>
                <p>{!!  $item->short_description  !!} <a href="/blog-details/{{ $item->blog_id }}" class="btn-default">READ MORE</a></p>
            </div>
        </div>
            @endforeach
    </div>
@endsection