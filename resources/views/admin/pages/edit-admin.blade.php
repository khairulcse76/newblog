@extends('admin.layouts.admin_master')
@section('title')Update @endsection
@section('main content')
@section('main content')
    <br>
    <br>
    <br>
    <div class="col-md-2"> </div>
    <div class="col-md-8">
        <div class="box box-info">
            <div class="box-header with-border">
                <h2 class="box-title">Update Admin Information</h2>
            </div>
            @if(session('massage'))
                <div class="form-group">
                    <div class="col-sm-12">
                        <div class="alert-success" style="font-size: large; padding: 2px;"><center>{{ session('massage') }}</center></div>
                    </div>
                </div><hr>
            @endif
            @if(session('errors'))
                <div class="form-group">
                    <div class="col-sm-12">
                        <div class="alert-danger" style="font-size: large; padding: 2px;"><center>(*) are Required</center></div>
                    </div>
                </div><hr>
            @endif
            <form class="form-horizontal" action="{{ URL::to('/admin/update-admin/'.$admin_info->admin_id)}}" method="post" enctype="multipart/form-data" >
                {{ csrf_field() }}
                <div class="box-body">
                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-2 control-label">Full Name<span style="color: red;">*</span></label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="admin_name" id="admin_name" value="{{ $admin_info->admin_name }}{{ old('admin_name') }}" placeholder=" FullName">
                            @if($errors->has('admin_name'))
                                <span style="color:red;">{{ $errors->first('admin_name') }}</span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="admin_email" class="col-sm-2 control-label">Email<span style="color: red;">*</span></label>

                        <div class="col-sm-10">
                            <input type="email" class="form-control" name="admin_email" id="admin_email" value="{{ $admin_info->admin_email }}{{ old('admin_email') }}" placeholder="Enter Email Address">
                            @if($errors->has('admin_email'))
                                <span style="color:red;">{{ $errors->first('admin_email') }}</span>
                            @endif
                            @if(session('email_match'))
                                <span style="color:red;">{{ session('email_match') }}</span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="profilePicture" class="col-sm-2 control-label">Change Profile Picture</label>

                        <div class="col-sm-10">
                            <input type="file" class="form-control" name="profile_Picture" id="profile_Picture">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="Permission" class="col-sm-2 control-label">Permission<span style="color: red;">*</span></label>
                        <div class="col-sm-10">
                            <select class="form-control" id="access_label" name="access_label" Selected="{{ $admin_info->access_label }}{{ old('access_label')}}">
                                <option value="">Select Accessibility</option>
                                @if($admin_info->access_label == 1)
                                <option selected value="1">Permited</option>
                                <option value="0">Unpermited</option>
                                @endif
                                @if($admin_info->access_label == 0)
                                    <option  value="1">Permited</option>
                                    <option selected value="0">Unpermited</option>
                                @endif
                            </select>
                            @if($errors->has('access_label'))
                                <span style="color:red;">{{ $errors->first('access_label') }}</span>
                            @endif
                        </div>
                    </div>
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                    <a href="{{ URL::to('admin/manage-admin/') }}" class="btn btn-default">Cancel</a>
                    <button type="submit" class="btn btn-info pull-right">Update</button>
                </div>
                <!-- /.box-footer -->
            </form>
        </div>
    </div>
    <div class="col-md-2">   </div>
@endsection