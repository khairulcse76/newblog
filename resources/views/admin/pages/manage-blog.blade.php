@extends('admin.layouts.admin_master')
@section('title')Manage Blog @endsection
@section('main content')
    <section class="content">
        <div class="row">
            <div class="col-lg-2">

            </div>
            <div class="col-lg-8">
                <div class="box">
                    <div class="box-header">
                        <center><h3 class="" style="text-align: center; color: red;"> Manage Blog</h3></center>
                    </div>
                    <div class="box-body">
                        <center><tr>
                                <td colspan="5" style="text-align: center; font-size: 50px">
                                    @if(session('massage'))
                                        <div><span style="text-align: center; color: blue;">{{ session('massage') }}</span></div>
                                    @endif
                                </td>
                            </tr></center>
                        <table id="example2" class="table table-bordered table-hover">
                            <thead>
                            <tr>
                                <th style="text-align: center;">SL.</th>
                                <th style="text-align: center;">Title</th>
                                <th style="text-align: center;">Status</th>
                                <th colspan="2" style="text-align: center;">Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php $id=1 ?>
                            @foreach($all_blog_info->all() as $v_blog)
                                <tr>
                                    <td><?php echo $id++; ?></td>
                                    <td>{{$v_blog->blog_title}}
                                    </td>

                                    @if($v_blog->publication_status==1)
                                        <td>Published</td>
                                    @else
                                        <td style="color: red;">Unpublished</td>
                                    @endif

                                    <td colspan="2" style="text-align: center;">
                                        @if($v_blog->publication_status==1)
                                            <a class="btn btn-danger"style="margin-right: 5px" href="/admin/unpublish-blog/{{ $v_blog->blog_id }}">Unpublished</a>
                                        @else
                                            <a class="btn btn-success" style="width: 100px; margin-right: 5px" href="/admin/publish-blog/{{ $v_blog->blog_id}}">Published</a>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach

                            </tbody>
                        </table>

                    </div>
                </div>
            </div>
            <div class="col-lg-2"></div>
        </div>
    </section>

@endsection