@extends('admin.layouts.admin_master')
@section('title')Admin Management @endsection
@section('main content')
    <section class="content">
        <div class="row">
            <div class="col-lg-2"></div>
            <div class="col-lg-8">
                <div class="box">
                    <div class="box-header">
                        <center><h3 class="" style="text-align: center; color: red;"> Admin Management</h3></center>
                    </div>
                    <div class="box-body">
                        <center><tr>
                                <td colspan="5"  style="text-align: center; font-size: 50px">
                                    @if(session('massage'))
                                        <div class="alert-success"><span style="text-align: center; font-size: large; padding: 5px;">{{ session('massage') }}</span></div>
                                    @endif
                                </td>
                            </tr></center>
                        <table id="example2" class="table table-bordered table-hover">
                            <thead>
                            <tr>
                                <th style="text-align: center;">SL.</th>
                                <th style="text-align: center;">Title</th>
                                <th style="text-align: center;">Status</th>
                                <th style="text-align: center;">Status</th>
                                <th style="text-align: center;">Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php $id=1 ?>
                            @foreach($admin_info->all() as $admin)
                                <tr>
                                    <td><?php echo $id++; ?></td>
                                    <td>
                                        @if($admin->profile_Picture != NULL)
                                            <img class="img-thumbnail" width="50"   src="{{ asset($admin->profile_Picture) }}" alt="User Avatar">
                                        @endif
                                        @if($admin->profile_Picture == NULL)
                                            <img class="img-thumbnail" width="50"   src="{{ asset('admin_image/deafult.jpg') }}">
                                        @endif
                                        {{$admin->admin_name}}
                                    </td>

                                    @if($admin->access_label==1)
                                        <td>
                                            <a class="btn btn-success "style="margin-right: 5px; width: 100%" href="/admin/unauthorized-admin/{{ $admin->admin_id }}">Authorized</a>

                                        </td>
                                    @else
                                        <td style="color: red;">
                                            <a class="btn btn-danger "style="margin-right: 5px;width: 100%; " href="/admin/authorized-admin/{{ $admin->admin_id }}">Unauthorized</a> </td>
                                    @endif
                                    <td></td>
                                    <td>
                                        <a class="btn btn-success glyphicon glyphicon-eye-open"style="margin-right: 5px" href="/admin/view-admin/{{ $admin->admin_id }}"></a>
                                        <a class="btn btn-success glyphicon glyphicon-edit"style="margin-right: 5px" href="/admin/edit-admin/{{ $admin->admin_id }}"></a>
                                        <a class="btn btn-success glyphicon glyphicon-trash"style="margin-right: 5px; float: right" href="/admin/delete-admin/{{ $admin->admin_id }}" onclick="return check_delete()"></a>
                                    </td>
                                </tr>
                            @endforeach

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="col-lg-2"></div>
        </div>
    </section>

@endsection