@extends('admin.layouts.admin_master')
@section('title')View Admin info @endsection
@section('main content')
    <div class="col-md-2"></div>
<div class="col-md-8">
    <!-- Widget: user widget style 1 -->
    <div class="box box-widget widget-user-2">
        <!-- Add the bg color to the header using any of the bg-* classes -->
        <div class="widget-user-header bg-black-active">
            <div class="widget-user-image">
                @if($admin_info->profile_Picture != NULL)
                <img class="img-circle" src="{{ asset($admin_info->profile_Picture) }}" alt="User Avatar">
                @endif
                @if($admin_info->profile_Picture == NULL)
                <img class="img-circle" src="{{ asset('admin_image/deafult.jpg') }}" alt="User Avatar">
                @endif
            </div>
            <!-- /.widget-user-image -->
            <h3 class="widget-user-username">{{ $admin_info->admin_name }}</h3>
            <h5 class="widget-user-desc">Lead Developer</h5>
        </div>
        <div class="box-footer no-padding">
            <ul class="nav nav-stacked">
                <li><a href="#">Email <span class="pull-right ">{{ $admin_info->admin_email }}</span></a></li>
                <li><a href="#">password <span class="pull-right  ">{{ md5($admin_info->admin_password )  }}</span></a></li>
                <li><a href="#">Profile Picture<span class="pull-right ">{{ $admin_info->profile_Picture }}</span></a></li>
                <li><a href="#">Authorization <span class="pull-right badge bg-red">
                            @if($admin_info->access_label==1)
                               <samp> Authorized</samp>
                            @else
                             Unauthorized
                            @endif
                        </span></a></li>
            </ul>
        </div>
        <div class="box-footer">
            <a href="{{ URL::to('admin/manage-admin/') }}" class="btn btn-flickr">Back</a>
        </div>
    </div>
    <!-- /.widget-user -->

</div>
    <div class="col-md-2"></div>
    @endsection