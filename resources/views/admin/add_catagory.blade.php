@extends('admin.layouts.admin_master')
@section('title')Add Category @endsection
@section('main content')
    <div class="col-md-2"> </div>
    <div class="col-md-8">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="success " style="text-align: center"><span style="color:green;">Insert Category</span></h3>


                    @if(session('massage'))
                    <h3 style="color:blue">{{ session('massage') }}</h3>
                    @endif

                {!! Form::open(['url' => '/admin/save-category']) !!}
                <div class="box-body">
                    <div class="form-group">
                        <label for="exampleCategoryName">Category Name</label>
                        <input type="text" class="form-control" name="category_name" id="category_name" value="{{ old('category_name') }}" placeholder="Category Name">
                    @if($errors->has('category_name'))
                     <span style="color: red;">{{ $errors->first('category_name') }}</span>
                     @endif
                    </div>
                    <div class="form-group">
                        <label for="Description">Description</label>
                        <textarea name="category_description" class="form-control" row="5" id="ck_edotor" cols="30">{{ old('category_description') }}</textarea>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputFile">publication Status</label>
                        <select class="form-control" name="publication_status" value="{{ old('publication_status') }}">
                            {{--<option>Select Status</option>--}}
                            <option value="">Select Status</option>
                            <option value="1">Published</option>
                            <option value="0">Unpublished</option>
                        </select>
                        @if($errors->has('publication_status'))
                            <span style="color: red;">{{ $errors->first('publication_status') }}</span>
                        @endif
                    </div>
                    <div class="checkbox">
                        <label>
                            <input type="checkbox"> Check me out
                        </label>
                    </div>
                </div>
                <!-- /.box-body -->

                <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
                {!! Form::close() !!}
            </div>

        </div>

    </div>
    <div class="col-md-2">   </div>
@endsection

