@extends('admin.layouts.admin_master')
@section('title')Manage Category @endsection
@section('main content')
    <section class="content">
        <div class="row">
            <div class="col-lg-2"></div>
            <div class="col-lg-8">
                <div class="box">
                    <div class="box-header">
                        <center><h3 class="" style="text-align: center; color: red;"> Category View</h3></center>
                    </div>
                    <div class="box-body">
                        <center><tr>
                                <td colspan="5" style="text-align: center; font-size: 50px">
                                    @if(session('massage'))
                                        <div><span style="text-align: center; color: blue;">{{ session('massage') }}</span></div>
                                    @endif
                                </td>
                            </tr></center>
                        <table id="example2" class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th style="text-align: center;">SL.</th>
                                    <th style="text-align: center;">Name</th>
                                    <th style="text-align: center;">Status</th>
                                    <th colspan="2" style="text-align: center;">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php $id=1 ?>
                            @foreach($all_category->all() as $v_category)
                                <tr>
                                    <td><?php echo $id++; ?></td>
                                    <td>{{$v_category->category_name}}</td>

                                        @if($v_category->publication_status==1)
                                            <td>Published</td>
                                        @else
                                            <td style="color: red;">Unpublished</td>
                                        @endif

                                    <td colspan="2" style="text-align: center;">
                                    @if($v_category->publication_status==1)
                                            <a class="btn btn-danger"style="margin-right: 5px" href="/admin/unpublish-category/{{ $v_category->id }}">Unpublished</a>
                                    @else
                                        <a class="btn btn-success" style="width: 100px; margin-right: 5px" href="/admin/publish-category/{{ $v_category->id}}">Published</a>
                                    @endif

                                        <a class="btn btn-success" style="; margin-right: 2px" href="{{ URL::to('/admin/view-category/'.$v_category->id) }}">View</a>
                                        <a class="btn btn-success" style="" href="/admin/edit-category/{{ $v_category->id }}">Edit</a>
                                        <a class="btn btn-success" style="" href="/admin/delete-category/{{ $v_category->id }}" onclick="return check_delete()">Delete</a>
                                    </td>
                                </tr>
                            @endforeach

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="col-lg-2"></div>
        </div>
    </section>

@endsection