@extends('admin.layouts.admin_master')
@section('title')Add Blog @endsection
{{--<script src="https://cloud.tinymce.com/stable/tinymce.min.js"></script>--}}
{{--<script>--}}
    {{--tinymce.init({--}}
        {{--selector: 'textarea',--}}
        {{--theme: 'modern',--}}
    {{--});--}}
{{--</script>--}}
@section('main content')
    <!-- Content Header (Page header) -->
    <div class="box-body">

        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <h3 class="bg-gray-active text-primary text-center" style="font-family: monospace; font-weight: bold; padding: 5px;">Add A New Blog</h3>
            </div>
            @if(session()->has('message'))
            <div class="col-md-8 col-md-offset-2">
                <h3 class="bg-success text-primary text-center" style="font-family: monospace; font-weight: bold; padding: 5px;">
                    <span style="color:blue; padding: 5px"> {{session('message')}}</span>
                @endif
                </h3>
            </div>
            {!! Form::open(['url' => '/admin/save-blog','method'=>'post','enctype'=>'multipart/form-data']) !!}
                
                <div class="col-md-8 col-md-offset-2">

                    <div class="form-group">
                        <label> Blog Title </label>
                        <input type="text" class="form-control" name="blog_title" placeholder=" Blog Title">
                        @if($errors->has('blog_title'))
                            <span style="color:red;"> {{ $errors->first('blog_title') }}</span>
                            @endif
                    </div>
                    
                     <div class="form-group">
                        <label> Blog Category </label>
                        <select class="form-control" name="category_id">
                            <option value="">Select category</option>
                            @foreach($category as $item)
                            <option value="{{ $item->id }}">{{ $item->category_name }}</option>
                           @endforeach


                             {{--@foreach($all_published_category as $v_category)--}}
					{{----}}
                                             {{--<option value="{{$v_category->category_id}}">{{$v_category->category_name}}</option>--}}
					{{----}}
                             {{--@endforeach--}}
                        </select>
                         @if($errors->has('category_id'))
                             <span style="color:red;"> {{ $errors->first('category_id') }}</span>
                         @endif
                    </div>
<<<<<<< HEAD
=======

                    {{--<div class="form-group">--}}
                        {{--<label> Short Description </label>--}}
                        {{--<div class="box">--}}
                            {{--<div>--}}
                                {{--<div class="box-body pad">--}}
                                    {{--<form>--}}
                                        {{--<textarea id="short_description" name="short_description" class="form-control ck_editor" placeholder="Place some text here" style="width: 100%; height: 100px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"></textarea>--}}
                                        {{--@if($errors->has('short_description'))--}}
                                            {{--<span style="color:red;"> {{ $errors->first('short_description') }}</span>--}}
                                        {{--@endif--}}
                                    {{--</form>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
>>>>>>> fe9aaac35b86ada5b6abcf2f3cac2093da99c4c4
                    <div class="form-group">
                        <label> Short Description </label>
                        <textarea id="short_description" name="short_description" class="form-control ck_editor" placeholder="Place some text here" style="width: 100%; height: 100px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"></textarea>
                        @if($errors->has('short_description'))
                            <span style="color:red;"> {{ $errors->first('short_description') }}</span>
                        @endif
                    </div>
                    <div class="form-group">
                        {{--<label> Long Description </label>--}}
                        <div class="box">
                            <div class="box-header">
                                <h3 class="box-title">Long Description</h3>
                                <!-- tools box -->
                                <div class="pull-right box-tools">
                                    <button type="button" class="btn btn-default btn-sm" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                                        <i class="fa fa-minus"></i></button>
                                    <button type="button" class="btn btn-default btn-sm" data-widget="remove" data-toggle="tooltip"
                                            title="Remove">
                                        <i class="fa fa-times"></i></button>
                                </div>
                                <!-- /. tools -->
                            </div>
                            <!-- /.box-header -->
                            <div class="box-body pad">
                                <form>
                                    <textarea name="long_description"  id="long_description" class="textarea" placeholder="Place some text here"
                                    style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"></textarea>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label> Blog Image </label>
                        <input type="file" name="blog_image" >
                    </div>
                    <div class="form-group">
                        <label> Publication Ststus </label>
                        <select class="form-control" name="publication_status">
                            <option value=""> Select Status</option>
                            <option value="1">Published</option>
                            <option value="0">Unpublished</option>
                        </select>
                        @if($errors->has('publication_status'))
                            <span style="color:red;"> {{ $errors->first('publication_status') }}</span>
                        @endif
                    </div>
                    <input class="btn btn-success" type="submit" name="submit" value="Submitt" style="float: right">
                </div>
           {!! Form::close() !!}
        </div>
        <!-- /.row -->


    </div>

 @endsection