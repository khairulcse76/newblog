@extends('layouts.master')
<link rel="stylesheet" href="{{ asset('admin-asset/dist/css/bootstrap.min.css') }}">
<link rel="stylesheet" href="{{ asset('admin-asset/font-awesome/css/font-awesome.min.css') }}">
<link rel="stylesheet" href="{{ asset('admin-asset/Ionicons/css/ionicons.min.css') }}">
<link rel="stylesheet" href="{{ asset('admin-asset/dist/css/AdminLTE.min.css') }}">
<link rel="stylesheet" href="{{ asset('admin-asset/dist/css/_all-skins.min.css') }}">
@section('main_content')

    <div class="content-grid">

            <div class="content-grid-info">
                <img src="{{ asset($blog_info->blog_image) }}" width="600" height="325" alt=""/>
                <div class="post-info">
                    <h4><a href="/blog-details/{{ $blog_info->blog_id }}">{!! $blog_info->blog_title !!}  </a> July 30, 2014 / 27 {{ $blog_info->category_name }}</h4>
                    <p>{!!  $blog_info->long_description !!}</p>
                </div>
                <div class="box-footer box-comments">
                    <div class="box-comment">
                        <img class="img-circle img-sm" src="{{ asset('/admin-asset/dist/img/user3-128x128.jpg') }}" alt="User Image">
                        <div class="comment-text">
                      <span class="username">
                        Maria Gonzales
                        <span class="text-muted pull-right">8:03 PM Today</span>
                      </span ><!-- /.username -->
                            <p id="comment_view">It is a long established fact that a reader will be distracted
                                by the readable content of a page when looking at its layout.</p>
                        </div>
                    </div>
                </div>
                <div class="box-footer">
                    {{--<form >--}}{{--laksdjlka lkdfj dfdnfkjdf kdjsasd fkjdfj dkfd--}}
                    <form action="/comment/{{ $blog_info->blog_id }}" method="post" method="post">
                        {{ csrf_field() }}
                        <img class="img-responsive img-circle img-sm" src="{{ asset('/admin-asset/dist/img/user1-128x128.jpg') }}" alt="Alt Text">
                        <div class="img-push">
                            <input type="text" class="form-control input-sm" id="comment" name="comment" placeholder="Press enter to post comment">
                            <input type="submit" class="btn btn-success btn-sm" onclick="return comment()">
                        </div>
                    </form>
                </div>
            </div>
    </div>
@endsection


{{--<script>--}}
    {{--function comment() {--}}

        {{--var comment=document.getElementById('comment').value;--}}
        {{--var dataString='comment' + comment;--}}
        {{--$.ajax({--}}
            {{--type:'post',--}}
            {{--url:"/comment/{{ $blog_info->blog_id }}",--}}
            {{--data:dataString,--}}
            {{--cache:false,--}}
            {{--success:function (thml) {--}}
                {{--$('#comment_view').html(thml);--}}
            {{--}--}}

        {{--})--}}
        {{--return false--}}
    {{--}--}}
{{--</script>--}}