<?php

namespace App\Http\Controllers;

use App\Category;
use App\Http\Requests\addCategory;
//use Illuminate\Contracts\Validation\Validator;
use Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Session;
use DB;
session_start();

class SuperAdminController extends Controller
{
    public function authCheck(){
        $admin_id=Session::get('admin_id');
        if ($admin_id == NULL){
            return Redirect::to('/admin')->send();
        }
    }
    public function index()
    {
        $this->authCheck();
        return view('admin.dashboard');
    }
    public function logout()
    {
        Session::put('admin_id', '');
        Session::put('admin_name', '');
        Session::put('massage', 'You are successfully Logout.');
        return Redirect::to('/admin');
    }
    public function add_catagory()
    {

        return view('admin.add_catagory');
    }
    public function manage_catagory()
    {
        $all_category=DB::table('category')->get();

        return view('/admin/manage_catagory')->with('all_category', $all_category);
    }

     public function saveCategory(Request $request)
    {
        $this->validate($request,[
            'category_name' => 'required',
            'publication_status' => 'required',
        ]);

        $data=array();
        $data['category_name']=$request->category_name;
        $data['category_description']=$request->category_description;
        $data['publication_status']=$request->publication_status;
        $insert_info=DB::table('category')->insert($data);

        if ($insert_info){
            session::flash('massage', 'Category successful inserted');
            return Redirect::back();
        }
    }
    public function publishCategory($categoryId){

        $data=array();
        $data['publication_status']=1;
        $update=DB::table('category')
            ->where('id', $categoryId)
            ->update($data);
        if ($update){
            session()->flash('massage', 'Category Unpublished Successful');
            return Redirect::back();
        }
    }
    public function unpublishCategory($categoryId){

        $data=array();
        $data['publication_status']=0;
                $update=DB::table('category')
                    ->where('id', $categoryId)
                    ->update($data);
                if ($update){
                    session()->flash('massage', 'Category Published Successful');
                    return Redirect::back();
                }
        }
public function singleViewCategory($categoryId){
                $cat_info=DB::table('category')
                    ->where('id', $categoryId)
                    ->first();
//                print_r($cat_info);
//                exit();

                    return view('admin/pages/view-category')->with('cat_info',$cat_info);
        }

    public function editCategory($categoryId){

//    echo $categoryId;
//        exit();
        $cat_info=DB::table('category')
            ->where('id', $categoryId)
            ->first();
//                print_r($cat_info);
//                exit();

        return view('admin/pages/edit-category')->with('cat_info',$cat_info);
    }
    public function updateCategory(Request $request,$categoryId){

        $this->validate($request,[
            'category_name' => 'required',
            'publication_status' => 'required',
        ]);
        $cat_info=DB::table('category')
            ->where('id', $categoryId)
            ->update(['category_name' => $request->category_name, 'category_description' => $request->category_description,
                'publication_status' => $request->publication_status]);
        session()->flash('massage', 'Category Information successfully updated');
        return redirect('/admin/manage_catagory');
    }



    public function addBlog()
    {
        $cat_info=DB::table('category')->get();

        return view('/admin.add-blog')->with('category', $cat_info);
    }
    public function manageBlog()
    {
//        echo "askdja"; exit();

        $all_blog= DB::table('tbl_blog')->orderBy('blog_id', 'desc')->get();

        return view('admin/pages/manage-blog')
            ->with('all_blog_info',$all_blog);
    }
    public function saveBlog(Request $request)
    {
        $this->validate($request,[
            'blog_title' => 'required|max:250',
            'category_id' => 'required',
            'short_description' => 'required',
            'publication_status' => 'required',
        ]);
        $data=array();
        $data['category_id']=$request->category_id;
        $data['admin_id']=Session::get('admin_id');
        $data['blog_title']=$request->blog_title;
        $data['short_description']=$request->short_description.'..................';
        $data['long_description']=$request->long_description;
        $data['publication_status']=$request->publication_status;
        $data['hit_count']=0;

        $image = $request->file('blog_image');
        if ($image) {
            $image_name = str_random(20);
            $ext = strtolower($image->getClientOriginalExtension());
            $image_full_name = $image_name . '.' . $ext;
            $upload_path = 'blog_image/';
            $image_url = $upload_path . $image_full_name;
            $success = $image->move($upload_path, $image_full_name);
            if ($success) {
                $data['blog_image'] = $image_url;
                DB::table('tbl_blog')->insert($data);
                session()->flash('message', 'Save blog Information Successfully !');
                return back();
            }
        }
        else{
            DB::table('tbl_blog')->insert($data);
            session()->flash('message', 'Save blog Information Successfully !');
            return back();
        }


    }



    public function destroyCategory($id)
    {
        $delete_info=DB::table('category')
            ->where('id', $id)
            ->delete();
        session()->flash('massage', 'Category Delete successful');
        return back();
    }
    public function publishBlog($blogId){
        $data=array();
        $data['publication_status']=1;
        $update=DB::table('tbl_blog')
            ->where('blog_id', $blogId)
            ->update($data);
        if ($update){
            session()->flash('massage', 'Blog Published Unsuccessful');
            return Redirect::back();
        }
    }
    public function unpublishBlog($blogId){
        $data=array();
        $data['publication_status']=0;
        $update=DB::table('tbl_blog')
            ->where('blog_id', $blogId)
            ->update($data);
        if ($update){
            session()->flash('massage', 'Blog Published Successful');
            return Redirect::back();
        }
    }


    public function addAdmin(){
//        echo "from addAdmin function";

        return view('admin/pages/add-admin');
    }
}


