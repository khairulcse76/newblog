<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Redirect;

class BlogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function commentSave(Request $request)
    {
        $name= $request->comment;;
        echo $name; exit();
    }

    public function index()
    {
        $blog_info = DB::table('tbl_blog')
            ->join('category', 'category.id', '=', 'tbl_blog.category_id')
            ->where('tbl_blog.publication_status',1)
            ->select('tbl_blog.*', 'category.category_name')
            ->orderBy('tbl_blog.blog_id', 'desc')
            ->get();
        $sidebar=1;

        return view('/home')
            ->with('sidebar', $sidebar)
            ->with('blog_info', $blog_info);
    }
    public function aboutUs()
    {
        $sidebar='';
        return view('pages.about')
            ->with('sidebar', $sidebar);
    }
    public function contactUs()
    {
        $sidebar='';
        return view('pages.contact')
            ->with('sidebar', $sidebar);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function blogDetails($id)
    {
        $blog_info = DB::table('tbl_blog')
            ->join('category', 'category.id', '=', 'tbl_blog.category_id')
            ->where('tbl_blog.blog_id',$id)
            ->select('tbl_blog.*', 'category.category_name')
            ->first();
        $sidebar=1;

        if ($blog_info){
            DB::table('tbl_blog')->where('blog_id',$id)->increment('hit_count');
        }
        return view('/blog-details')
            ->with('sidebar', $sidebar)
            ->with('blog_info', $blog_info);
    }


    public function allCategory($id){
        $allcategory=DB::table('tbl_blog')
                    ->where('category_id', $id)
                    ->orderBy('hit_count', 'desc')
                    ->get();

        $sidebar=1;

        if ($allcategory){
            DB::table('tbl_blog')->where('blog_id',$id)->increment('hit_count');
        }
        return view('/category-details')
            ->with('sidebar', $sidebar)
            ->with('category_details', $allcategory);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \\IlluminateHttp\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function editor()
    {
        return view('admin.pages.editor');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
