<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use Session;
use Validator;
session_start();



class AdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function authStatus(){
        $admin_id=session::get('admin_id');

        if ($admin_id != NULL){
            return Redirect::to('/admin/dashboard')->send();
        }
    }
    public function index()
    {
        $this->authStatus();
        return view('admin.admin_login');
    }


public function adminLoginCheck(Request $request)
    {
        $email_address = $request->admin_email;
        $password = $request->admin_password;

//        echo $email_address .'------------'.$password;

        $admin_info = DB::table('tbl_admin')
            ->select('*')
            ->where('admin_email', $email_address)
            ->where('admin_password', md5($password))
            ->first();

        if ($admin_info){

            Session::put('admin_id', $admin_info->admin_id);
            Session::put('admin_name', $admin_info->admin_name);
            Session::put('profile_picture', $admin_info->profile_Picture);
            Session::put('exception', '');

            return Redirect::to('/admin/dashboard');
        }else{
            if ($email_address== NULL || $password == NULL){
                if ($email_address == NULL){
                    Session::put('exception', 'Email Address Required!');
                    return Redirect::to('/admin');
                }else if ($password == NULL){
                    Session::put('exception', 'You must need your password');
                    return Redirect::to('/admin');
                }
            }else {
                Session::put('exception', 'Email or Password Invalid..!');
                return Redirect::to('/admin');
            }
        }

    }


    public function storeAdmin(Request $request)
    {
//        $item=$request['confirm_password'];
//        echo $item; exit();
        $this->validate($request, [
          'admin_name'=>'required',
          'admin_email'=>'required',
          'admin_password'=>'required',
          'access_label'=>'required',
       ]);

        $data=array();
        $data['admin_name']=$request->admin_name;
        $data['admin_email']=$request->admin_email;
        $data['admin_password']=md5($request->admin_password);
        $data['access_label']=$request->access_label;
        $image = $request->file('profile_Picture');

        if ($request['admin_password'] !=$request['confirm_password']){
            session()->flash('confirm', 'Confirm password dosnt match');
            return back()->withInput();
        }else{
            $email_match=DB::table('tbl_admin')->where('admin_email', $request->admin_email)->first();
            if ($email_match){
                session()->flash('email_match', 'This email already exist');
                return back()->withInput();
            }else{
                if ($image) {
                    $image_name = str_random(20);
                    $ext = strtolower($image->getClientOriginalExtension());
                    $image_full_name = $image_name . '.' . $ext;
                    $upload_path = 'admin_image/';
                    $image_url = $upload_path . $image_full_name;
                    $success = $image->move($upload_path, $image_full_name);
                    if ($success) {
                        $data['profile_Picture'] = $image_url;
                        DB::table('tbl_admin')->insert($data);
                        session::flash('massage', 'Admin successful inserted');
                        return Redirect::back();
                    }
                }
                else{
                    DB::table('tbl_admin')->insert($data);
                    session::flash('massage', 'Admin successful inserted');
                    return Redirect::back();
                }
            }
        }
    }


    public function manageAdmin()
    {
        $admin_info=DB::table('tbl_admin')->get();
        return view('admin.pages.manage-admin')->with('admin_info', $admin_info);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function authorizationAdmin($id)
    {
        $updates= DB::table('tbl_admin')->where('admin_id', $id)->update(['access_label' => 1]);
        if ($updates){
            session()->flash('massage', 'Admin or user Authorization successful');
            return back();
        }
    }
 public function unauthorizationAdmin($id)
    {
        $updates= DB::table('tbl_admin')->where('admin_id', $id)->update(['access_label' => 0]);
        if ($updates){
            session()->flash('massage', 'Admin Un-Authorization successful');
            return back();
        }
    }
    public function editAdmin($id)
    {
        $edit= DB::table('tbl_admin')->where('admin_id', $id)->first();
        return view('admin.pages.edit-admin')->with('admin_info', $edit);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updateAdmin(Request $request, $id)
    {
//        $item=$request['confirm_password'];
//        echo $item; exit();
        $this->validate($request, [
            'admin_name'=>'required',
            'admin_email'=>'required',
            'access_label'=>'required',
        ]);

        $data=array();
        $data['admin_name']=$request->admin_name;
        $data['admin_email']=$request->admin_email;
        $data['access_label']=$request->access_label;
        $image = $request->file('profile_Picture');

            $email_match=DB::table('tbl_admin')->where('admin_email', $request->admin_email)->where('admin_id', $id)->first();
            if ($email_match){
                if ($image) {
                    $image_name = str_random(20);
                    $ext = strtolower($image->getClientOriginalExtension());
                    $image_full_name = $image_name . '.' . $ext;
                    $upload_path = 'admin_image/';
                    $image_url = $upload_path . $image_full_name;
                    $success = $image->move($upload_path, $image_full_name);
                    if ($success) {
                        $data['profile_Picture'] = $image_url;
                        DB::table('tbl_admin')->update($data);
                        session::flash('massage', 'Admin Information successfully Update');
                        return redirect()->route('admin/manage-admin');
                    }
                }
                else{
                    DB::table('tbl_admin')->update($data);
                    session::flash('massage', 'Admin Information successfully Update');
                    return Redirect::to('admin/manage-admin');
                }

            }else{
                session()->flash('email_match', 'This email already exist');
                return back();
            }
        }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function viewAdmin($id)
    {
        $admin_info=DB::table('tbl_admin')->where('admin_id', $id)->first();
//        session::flash('massage', 'Admin Information Successfully Delete');
        return view('admin.pages.view-admin')->with('admin_info', $admin_info);
    }
    public function destroyAdmin($id)
    {
        DB::table('tbl_admin')->where('admin_id', $id)->delete();
        session::flash('massage', 'Admin Information Successfully Delete');
        return back();
    }
}
